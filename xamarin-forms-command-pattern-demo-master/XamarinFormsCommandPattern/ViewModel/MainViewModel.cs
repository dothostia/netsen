﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;
namespace XamarinFormsCommandPattern.ViewModel
{
    public class MainViewModel: INotifyPropertyChanged
    {
        #region Command Property Declaration
        public ICommand saveCommand { get; private set; }
        public ICommand deleteCommand { get; private set; }
        #endregion

        #region Edit Text Property Declaration
        private string _noteText;
        public string NoteText
        {
            get { return _noteText; }
            set { _noteText = value; OnPropertyChanged("NoteText"); }
        }
        #endregion

        #region Page Level Variable Declaration
        public List<String> notesData; //For storing data on save button click
        #endregion

        #region Main Method
        public MainViewModel()
        {
            notesData = new List<string>();
            saveCommand = new Command(SaveNote);
            deleteCommand = new Command(DeleteNote);

        }
        #endregion

        #region Button Command
        #region Save Command
        public async void SaveNote()
        {
            if (NoteText != null)
            {
                notesData.Add(NoteText);
                await App.Current.MainPage.DisplayAlert("Sucess!", "Note saved successfully.", "OK");
            }

        }
        #endregion
        #region Delete Button
        public async void DeleteNote()
        {
            if (NoteText != null) //Check Note Content In Edit Area.
            {
                if (notesData.Contains(NoteText.Trim())) //Check Note Modification and Save Status.
                {
                    String ShotNoteText = NoteText.Substring(0, Math.Min(NoteText.Length, 20));//For displaying short note text in alert.
                    bool answer = await App.CurrentInstance.MainPage.DisplayAlert("Confirmation!", "Are you sure you want to delete (" + ShotNoteText.Trim() + ") this note?", "Yes", "No");
                    if (answer == true)
                    {
                        if (notesData.Count > 0)
                        {
                            notesData.Remove(NoteText); //Remove Specific Note.
                            NoteText = "";
                            await App.CurrentInstance.MainPage.DisplayAlert("Sucess!", "Note removed suscessfully", "OK");

                        }
                    }
                }
                else
                {
                    await App.CurrentInstance.MainPage.DisplayAlert("Warning!", "Note is not saved. Unable to delete note.", "OK");
                }
            }
            else
            {
                await App.CurrentInstance.MainPage.DisplayAlert("Warning!", "Note is empty.", "OK");
            }
        }
        #endregion
        #endregion

        #region Notification Property Chnaged Event
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
