﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using XamarinFormsCommandPattern.ViewModel;

namespace XamarinFormsCommandPattern
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            #region iPhone Safe Area Fix
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
            MainViewModel mainObjectModel = new MainViewModel();
            this.BindingContext = mainObjectModel;
            #endregion
        }
    }
}
