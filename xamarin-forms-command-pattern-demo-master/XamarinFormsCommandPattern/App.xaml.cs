﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinFormsCommandPattern
{

    public partial class App : Application
    {
        #region Property Declaration
        public static App CurrentInstance { get; set; }// Current Instance
        #endregion
    
        public App()
        {
            InitializeComponent();
            CurrentInstance = this; //Declaration of Current Instance
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
